package com.udacity.popularmovies.ui.movielist;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.udacity.popularmovies.R;
import com.udacity.popularmovies.utilities.NetworkUtils;
import com.udacity.popularmovies.model.Movie;
import com.udacity.popularmovies.utilities.ConnectionDetector;
import com.udacity.popularmovies.utilities.IntentTools;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MoviesAdapter.ListItemClickListener {

    private RecyclerView mMoviesList;
    private ProgressBar mProgressbarLoading;
    private TextView noConnectionTextView;
    private MoviesAdapter mAdapter;
    private ConnectionDetector connectionDetector;
    private List<Movie> movieList;
    private int selected = -1;
    MenuItem menuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            setTitle(savedInstanceState.getString("title"));
            selected = savedInstanceState.getInt("item_selected");
        }
        mProgressbarLoading = findViewById(R.id.progress_bar_loading);
        noConnectionTextView = findViewById(R.id.tv_no_connection);
        mMoviesList = findViewById(R.id.rv_movies);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        mMoviesList.setLayoutManager(gridLayoutManager);
        mMoviesList.setHasFixedSize(true);
        mAdapter = new MoviesAdapter(this);
        mMoviesList.setAdapter(mAdapter);
        connectionDetector = new ConnectionDetector(this);
        handleMovieListType();
    }

    private void handleMovieListType() {
        if (selected == R.id.action_favourite) {
            prepareFavouriteMovies();
        }  else if (selected == R.id.action_top_rated) {
            prepareTopMovies();
        }else {
            preparePopularMovies();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("title", getTitle().toString());
        outState.putInt("item_selected", selected);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        if (selected == -1) {
            return true;
        }
        switch (selected) {
            case R.id.action_popular:
                menuItem = (MenuItem) menu.findItem(R.id.action_popular);
                menuItem.setChecked(true);
                break;
            case R.id.action_top_rated:
                menuItem = (MenuItem) menu.findItem(R.id.action_top_rated);
                menuItem.setChecked(true);
                break;
            case R.id.action_favourite:
                menuItem = (MenuItem) menu.findItem(R.id.action_favourite);
                menuItem.setChecked(true);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_popular) {
            selected = id;
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            preparePopularMovies();
            return true;
        } else if (id == R.id.action_top_rated) {
            selected = id;
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            prepareTopMovies();
            return true;
        } else if (id == R.id.action_favourite) {
            selected = id;
            if (item.isChecked()) item.setChecked(false);
            else item.setChecked(true);
            prepareFavouriteMovies();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        IntentTools.goToMovieDetailsFromItem(this, movieList.get(clickedItemIndex).getId());
    }

    private void preparePopularMovies() {
        mProgressbarLoading.setVisibility(View.VISIBLE);
        mMoviesList.setVisibility(View.GONE);
        setTitle(R.string.popular_title);
        if (connectionDetector.isConnectingToInternet()) {
            mProgressbarLoading.setVisibility(View.VISIBLE);
            noConnectionTextView.setVisibility(View.INVISIBLE);
            new GetMoviesList().execute(NetworkUtils.buildMovieUrl(getString(R.string.popular)));
        } else {
            mProgressbarLoading.setVisibility(View.INVISIBLE);
            noConnectionTextView.setVisibility(View.VISIBLE);
        }
    }

    private void prepareTopMovies() {
        mProgressbarLoading.setVisibility(View.VISIBLE);
        mMoviesList.setVisibility(View.GONE);
        setTitle(R.string.top_rated_title);
        if (connectionDetector.isConnectingToInternet()) {
            mProgressbarLoading.setVisibility(View.VISIBLE);
            noConnectionTextView.setVisibility(View.INVISIBLE);
            new GetMoviesList().execute(NetworkUtils.buildMovieUrl(getString(R.string.top_rated)));
        } else {
            mProgressbarLoading.setVisibility(View.INVISIBLE);
            noConnectionTextView.setVisibility(View.VISIBLE);
        }
    }

    private void prepareFavouriteMovies() {
        setTitle(R.string.favourite_title);
        MainViewModel viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
//        LiveData<List<Movie>> favouriteList =  mDb.movieDao().loadAllMovies();
        viewModel.getMovies().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movieArrayList) {
                if (getTitle().equals(getString(R.string.favourite_title))) {
                    movieList = movieArrayList;
                    if (movieList.size() == 0) {
                        noConnectionTextView.setText(R.string.empty_list);
                        noConnectionTextView.setVisibility(View.VISIBLE);
                        mMoviesList.setVisibility(View.GONE);
                    } else {
                        noConnectionTextView.setVisibility(View.GONE);
                        mMoviesList.setVisibility(View.VISIBLE);
                        mAdapter.setList(movieList);
                    }
                }
            }
        });
    }


    class GetMoviesList extends AsyncTask<URL, Void, ArrayList<Movie>> {

        @Override
        protected ArrayList<Movie> doInBackground(URL... params) {
            URL movieUrl = params[0];
            String moviesJson = null;
            ArrayList<Movie> list = null;
            try {
                moviesJson = NetworkUtils.getResponseFromHttpUrl(movieUrl);
                list = NetworkUtils.getArrayFromJson(moviesJson, getString(R.string.movie_results));

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<Movie> list) {
            mProgressbarLoading.setVisibility(View.GONE);

            if (list != null && list.size() != 0) {
                movieList = list;
                mMoviesList.setVisibility(View.VISIBLE);
                mAdapter.setList(list);
            }
        }
    }


}
