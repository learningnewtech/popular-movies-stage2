package com.udacity.popularmovies.ui.movie;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.udacity.popularmovies.R;
import com.udacity.popularmovies.data.database.AppDatabase;
import com.udacity.popularmovies.model.Movie;
import com.udacity.popularmovies.model.MovieReview;
import com.udacity.popularmovies.model.MovieTrailer;
import com.udacity.popularmovies.ui.ReviewsAdapter;
import com.udacity.popularmovies.ui.TrailerAdapter;
import com.udacity.popularmovies.utilities.AppExecutors;
import com.udacity.popularmovies.utilities.ConnectionDetector;
import com.udacity.popularmovies.utilities.IntentTools;
import com.udacity.popularmovies.utilities.NetworkUtils;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;


public class MovieDetailsActivity extends AppCompatActivity
        implements TrailerAdapter.ListItemClickListener, ReviewsAdapter.ListReviewItemClickListener {

    private ConnectionDetector connectionDetector;
    private int movieId;
    private ProgressBar mProgressbarLoading;
    private ImageView moviePosterImageView;
    private TextView movieTitleTextView, overviewTextView;
    private RatingBar voteAverageRatingBar;
    private TabLayout tableLayout;
    private RecyclerView mTrailerList, mReviewList;
    private ArrayList<MovieTrailer> trailerArrayList;
    private ArrayList<MovieReview> reviewArrayList;
    private TrailerAdapter mTrailerAdapter;
    private ReviewsAdapter mReviewAdapter;
    private ConstraintLayout containerView;
    private TextView noConnectionTextView;

    private AppDatabase mDb;
    private Movie movieDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        if (getIntent() != null) {
            String key = getString(R.string.key_item_id);
            if (getIntent().hasExtra(key)) {
                movieId = getIntent().getIntExtra(key, -1);
            }
        }

        mDb = AppDatabase.getInstance(getApplicationContext());

        containerView = findViewById(R.id.container_view);
        mProgressbarLoading = findViewById(R.id.progress_bar_loading);
        noConnectionTextView = findViewById(R.id.tv_no_connection);
        moviePosterImageView = findViewById(R.id.iv_poster);
        movieTitleTextView = findViewById(R.id.tv_movie_title);
//        movieReleaseDateTextView = findViewById(R.id.tv_movie_release_date);
        voteAverageRatingBar = findViewById(R.id.vote_rating_bar);
        overviewTextView = findViewById(R.id.tv_overview);
        mTrailerList = findViewById(R.id.rv_trailers);
        LinearLayoutManager trailerLinearLayoutManager = new LinearLayoutManager(this);
        mTrailerList.setLayoutManager(trailerLinearLayoutManager);
        mTrailerList.setHasFixedSize(true);
        mReviewList = findViewById(R.id.rv_reviews);
        LinearLayoutManager reviewLinearLayoutManager = new LinearLayoutManager(this);
        mReviewList.setLayoutManager(reviewLinearLayoutManager);
        mReviewList.setHasFixedSize(true);
        prepareTabs();
        connectionDetector = new ConnectionDetector(this);
        if (connectionDetector.isConnectingToInternet()) {
            mProgressbarLoading.setVisibility(View.VISIBLE);
            noConnectionTextView.setVisibility(View.GONE);
            new GetMovie().execute(NetworkUtils.buildMovieDetailsUrl(movieId));
            new GetMovieTrailer().execute(NetworkUtils.buildMovieTrailerUrl(movieId));
            new GetMovieReviews().execute(NetworkUtils.buildMovieReviewUrl(movieId));
        } else {
            noConnectionTextView.setVisibility(View.VISIBLE);
            mProgressbarLoading.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);
        setFavouriteIcon(menu.getItem(0));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_add_favourite) {
            updateFavouriteIcon(item);
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateFavouriteIcon(final MenuItem item) {
        final LiveData<Movie> isExist = isMovieExist();
        isExist.observe(this, new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movie) {
                if (movie == null) {
                    saveMovie();
                    item.setIcon(R.drawable.ic_favorite);
                }else {
                    deleteMovie();
                    item.setIcon(R.drawable.ic_un_favorite);
                }
                isExist.removeObserver(this);
            }
        });
    }

    private LiveData<Movie> isMovieExist() {
        return mDb.movieDao().loadMovie(movieId);
//        if (movie == null) {
//            return false;
//        }
//        return true;
    }

    private void setFavouriteIcon(final MenuItem item) {
        final LiveData<Movie> isExist = isMovieExist();
        isExist.observe(this, new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movie) {
                if (movie == null) {
                    item.setIcon(R.drawable.ic_un_favorite);
                }else {
                    item.setIcon(R.drawable.ic_favorite);
                }
                isExist.removeObserver(this);
            }
        });
    }

    private void saveMovie() {
//        Movie favMovie = new Movie(movieId, movieTitleTextView.getText().toString(), movieDetails.getBackdrop_path());
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.movieDao().insertMovie(movieDetails);
            }
        });
    }

    private void deleteMovie() {
//        Movie favMovie = new Movie(movieId, movieTitleTextView.getText().toString(), movieDetails.getBackdrop_path());
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.movieDao().deleteMovie(movieDetails);
            }
        });
    }

    private void populateUI(Movie movie) {
        String imageUrl = getMovieImage(movie);
        Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.movie_placeholder)
                .error(R.drawable.movie_placeholder)
                .into(moviePosterImageView);
        String[] date = movie.getRelease_date().split("-");
        movieTitleTextView.setText(movie.getOriginal_title() + " (" + date[0] + ")");
//        movieReleaseDateTextView.setText(movie.getRelease_date());
        voteAverageRatingBar.setRating((movie.getVote_average() * 5) / 10);
        overviewTextView.setText(movie.getOverview());
    }

    @NonNull
    private String getMovieImage(Movie movie) {
        return NetworkUtils.buildImageUrl(movie.getPoster_path(), NetworkUtils.IMAGE_SIZE_DETAILS);
    }

    private void prepareTabs() {
        tableLayout = findViewById(R.id.tab_details);
        tableLayout.addTab(tableLayout.newTab().setText(R.string.trailer));
        tableLayout.addTab(tableLayout.newTab().setText(R.string.reviews));
        tableLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String tabName = tab.getText().toString();
                if (tabName.equals(getString(R.string.trailer))) {
                    mReviewList.setVisibility(View.GONE);
                    mTrailerList.setVisibility(View.VISIBLE);
                } else {
                    mReviewList.setVisibility(View.VISIBLE);
                    mTrailerList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void populateTrailerUI(ArrayList<MovieTrailer> trailerList) {
        trailerArrayList = trailerList;
        mTrailerAdapter = new TrailerAdapter(trailerList, this);
        mTrailerList.setAdapter(mTrailerAdapter);
    }

    private void populateReviewUI(ArrayList<MovieReview> reviewList) {
        reviewArrayList = reviewList;
        mReviewAdapter = new ReviewsAdapter(reviewArrayList, this);
        mReviewList.setAdapter(mReviewAdapter);
    }


    @Override
    public void onListItemClick(int clickedItemIndex) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="
                + trailerArrayList.get(clickedItemIndex).getKey()
                + "\"")));

    }

    @Override
    public void onListReviewItemClick(String clickedItemUrl) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(clickedItemUrl)));
//        IntentTools.goToReviewDetailsFromItem(this, clickedItemIndex);
    }

    class GetMovie extends AsyncTask<URL, Void, Movie> {

        @Override
        protected Movie doInBackground(URL... params) {
            URL movieUrl = params[0];
            String moviesJson;
            Movie movie = null;
            try {
                moviesJson = NetworkUtils.getResponseFromHttpUrl(movieUrl);
                movie = NetworkUtils.getMovie(moviesJson);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return movie;
        }

        @Override
        protected void onPostExecute(Movie movie) {
            mProgressbarLoading.setVisibility(View.GONE);
            containerView.setVisibility(View.VISIBLE);

            if (movie != null) {
                movieDetails = new Movie(movieId, movie.getOriginal_title()
                        , movie.getPoster_path());
                populateUI(movie);
            }
        }
    }

    class GetMovieTrailer extends AsyncTask<URL, Void, ArrayList<MovieTrailer>> {

        @Override
        protected ArrayList<MovieTrailer> doInBackground(URL... params) {
            URL movieUrl = params[0];
            String moviesJson;
            ArrayList<MovieTrailer> trailerList = null;
            try {
                moviesJson = NetworkUtils.getResponseFromHttpUrl(movieUrl);
                trailerList = NetworkUtils.getMovieTrailer(moviesJson, getString(R.string.movie_results));

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return trailerList;
        }

        @Override
        protected void onPostExecute(ArrayList<MovieTrailer> trailer) {
            if (trailer != null) {
                populateTrailerUI(trailer);
            }
        }
    }

    class GetMovieReviews extends AsyncTask<URL, Void, ArrayList<MovieReview>> {

        @Override
        protected ArrayList<MovieReview> doInBackground(URL... params) {
            URL movieUrl = params[0];
            String moviesJson;
            ArrayList<MovieReview> reviewList = null;
            try {
                moviesJson = NetworkUtils.getResponseFromHttpUrl(movieUrl);
                reviewList = NetworkUtils.getMovieReviews(moviesJson, getString(R.string.movie_results));

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return reviewList;
        }

        @Override
        protected void onPostExecute(ArrayList<MovieReview> reviewList) {
            if (reviewList != null) {
                populateReviewUI(reviewList);
            }
        }
    }


}
