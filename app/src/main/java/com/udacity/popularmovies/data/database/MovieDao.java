package com.udacity.popularmovies.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.udacity.popularmovies.model.Movie;

import java.util.ArrayList;
import java.util.List;


@Dao
public interface MovieDao {

    @Insert
    void insertMovie(Movie movie);

    @Query("SELECT * FROM movie WHERE id LIKE :id")
    LiveData<Movie> loadMovie(int id);

    @Query("SELECT * FROM movie")
    LiveData<List<Movie>> loadAllMovies();


    @Delete
    void deleteMovie(Movie movie);
}
